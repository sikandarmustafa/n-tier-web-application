﻿using System;
using System.Collections.Generic;
using DTO.DTO_Feature;
using DAL.Security;

namespace BAL.Security
{
    public class BAL_Feature: BAL_Interface<DTO_Add, DTO_Update, DTO_Delete, DTO_GetAll>
    {
        SEC_Feature ftr = new SEC_Feature();
        public List<DTO_GetAll> GetAll()
        {
            try
            {
                return ftr.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(DTO_Add feature)
        {
            try
            {
                feature.Feature_Name.Trim();
                feature.Feature_Url.Trim();

                if (!IsAlreadyExists(feature))
                {
                    var result = ftr.Add(feature);
                    if (result)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Error While Adding Feature!! Try Again later";
                    }
                }
                else
                {
                    return "Same Name or Feature URL Already Exists!!";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string Update(DTO_Update feature)
        {
            try
            {
                bool result = ftr.Update(feature);
                if (result)
                {
                    return "Success";
                }
                else
                {
                    return "Error while updating Feature!! Try again letter";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Delete(DTO_Delete feature)
        {
            try
            {
                var result = ftr.Delete(feature);
                if (result)
                {
                    return "Success";
                }
                else
                {
                    return "Error while Deleting Feature!! Try again letter";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                searchTerm.Trim();
                return ftr.Search(searchTerm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetById(int id)
        {
            try
            {
                return ftr.GetById(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public bool IsAlreadyExists(DTO_Add feature)
        {
            try
            {
                return ftr.IsAlreadyExists(feature);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
      
    }
}
