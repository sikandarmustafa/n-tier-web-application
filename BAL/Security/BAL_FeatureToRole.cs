﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DTO.DTO_Feature;
using DAL.Security;

namespace BAL.Security
{
    public class BAL_FeatureToRole
    {
        SEC_FeatureToRole FeatureToRole = new SEC_FeatureToRole();

        public string Add(DTO_FeatureToRole FR)
        {
            try
            {
                if (FR.AF_Id > 0 && FR.Role_Id > 0)
                {
                    bool result = FeatureToRole.Add(FR);
                    if (result)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Something Went wrong while adding Feature To Role!!";
                    }
                }
                else
                {
                    return "Invalid Selection!!!";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Delete(DTO_FeatureToRole FR)
        {
            try
            {
                if (FR.AF_Id > 0 && FR.Role_Id > 0)
                {
                    bool result = FeatureToRole.Delete(FR);
                    if (result)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Something Went wrong while Deleting !!";
                    }
                }
                else
                {
                    return "Invalid Selection!!!";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> GetFeaturesAndActionsForAllRoles()
        {
            try
            {
                return FeatureToRole.GetFeaturesAndActionsForAllRoles();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> GetFeaturesAndActionsForSpecificRole(int roleId)
        {
            try
            {
                return FeatureToRole.GetFeaturesAndActionsForSpecificRole(roleId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string MarkAll(int Role_id)
        {
            try
            {
                if (FeatureToRole.MarkAll(Role_id))
                {
                    return "Success";
                }
                else
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UnMarkAll(int Role_id)
        {
            try
            {
                if (FeatureToRole.UnMarkAll(Role_id))
                {
                    return "Success";
                }
                else
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
