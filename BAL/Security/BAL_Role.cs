﻿using System;
using System.Collections.Generic;
using DAL.Security;
using DTO.DTO_Role;

namespace BAL.Security
{
    public class BAL_Role: BAL_Interface<DTO_Add, DTO_Update, DTO_Delete, DTO_GetAll>
    {
        SEC_Role role = new SEC_Role();
       
        public List<DTO_GetAll> GetAll()
        {
            try
            {
                return role.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(DTO_Add ViewRoleData)
        {
            try
            {
                if(IsAlreadyExist(ViewRoleData.Role_Title))
                {
                    return "Role Already Exist with same Title";
                }
                else
                {
                    if(role.Add(ViewRoleData))
                    {
                        return "success";
                    }
                    else
                    {
                        return "Something went wrong while adding a new role Try again!!";
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DTO_GetAll GetById(int id)
        {
            try
            {
                return role.GetById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(DTO_Update roleData)
        {

            try
            {
                if (role.Update(roleData))
                {
                    return "Success";
                }
                else
                {
                    return "Error While deleting User Try again later!!!";
                }
            }
            catch (Exception ex)
            {
                return "Error Message: " + ex.Message.ToString();
            }

        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                return role.Search(searchTerm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Delete(DTO_Delete roledata)
        {
            try
            {
                if (role.Delete(roledata))
                {
                    return "Success";
                }
                else
                {
                    return "Error While deleting User Try again later!!!";
                }
            }
            catch (Exception ex)
            {
                return "Error Message: " + ex.Message.ToString();
            }

        }

        public bool IsAlreadyExist(string roledata)
        {
            try
            {
                return role.IsAlreadyExist(roledata);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}
