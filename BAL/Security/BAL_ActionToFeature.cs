﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO.DTO_Feature;
using DTO;
using DTO.DTO_Action;
using DAL.Security;

namespace BAL.Security
{
    public class BAL_ActionToFeature
    {
        SEC_ActionToFeature ActionToFeature = new SEC_ActionToFeature();

        public List<DTO.DTO_Feature.DTO_GetAll> GetAll()
        {
            try
            {
                return ActionToFeature.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(DTO_ActionToFeature FA)
        {
            try
            {
                if (FA.Feature_Id > 0 && FA.Action_Id > 0)
                {
                    if (!IsAlreadyExist(FA))
                    {
                        bool result = ActionToFeature.Add(FA);
                        if (result)
                        {
                            return "Success";
                        }
                        else
                        {
                            return "Something Went wrong while add action to feature!!";
                        }
                    }
                    else
                    {
                        return "Action To Feature Already Exist, Please Referesh The Page!!!";

                    }

                }
                else
                {
                    return "Invalid Selection!!!";

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string Delete(DTO_ActionToFeature FA)
        {
            try
            {
                if (FA.Feature_Id > 0 && FA.Action_Id > 0)
                {
                    bool result = ActionToFeature.Delete(FA);
                    if (result)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Something Went wrong while Deleting action to feature!!";
                    }
                }
                else
                {
                    return "Invalid Selection!!!";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool IsAlreadyExist(DTO_ActionToFeature FA)
        {
            try
            {
                return ActionToFeature.IsAlreadyExist(FA);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
