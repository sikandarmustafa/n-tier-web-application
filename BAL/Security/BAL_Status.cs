﻿using System;
using System.Collections.Generic;
using DTO.DTO_Status;
using DAL.Security;

namespace BAL.Security
{
    public class BAL_Status : BAL_Interface<DTO_Add, DTO_Update, DTO_Delete, DTO_GetAll>
    {
        SEC_Status status = new SEC_Status(); 

        public string Add(DTO_Add statusData)
        {
            try
            {
                statusData.Status_Title.Trim();

                if (!IsAlreadyExists(statusData))
                {
                    bool result = status.Add(statusData);
                    if (result)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Error While Adding Status!! Try Again later";
                    }
                }
                else
                {
                    return "Same Status Already Exists!!";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string Update(DTO_Update statusData)
        {
            try
            {
                bool result = status.Update(statusData);

                if (result)
                {
                    return "Success";
                }
                else
                {
                    return "Something Went wrong while Updating Status.!!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Delete(DTO_Delete statusData)
        {
            try
            {
                bool result = status.Delete(statusData);
                if (result)
                {
                    return "Success";
                }
                else
                {
                    return "Error while Deleting Feature!! Try again letter";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                return status.Search(searchTerm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetById(int id)
        {
            try
            {
                return status.GetById(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public List<DTO_GetAll> GetAll()
        {
            try
            {
                return status.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsAlreadyExists(DTO_Add statusdata)
        {
            try
            {
                return status.IsAlreadyExists(statusdata);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
