﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Security
{
    interface BAL_Interface<T, U, V, X>
    {
        string Add(T data);
        string Update(U data);
        string Delete(V data);
        List<X> GetAll();
        X GetById(int id);
        List<X> Search(string SearchTerm);
    }
}
