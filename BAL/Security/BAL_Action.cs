﻿using System;
using System.Collections.Generic;
using DTO.DTO_Action;
using DAL.Security;

namespace BAL.Security
{
    public class BAL_Action : BAL_Interface<DTO_Add,DTO_Update,DTO_Delete,DTO_GetAll>
    {
        SEC_Action act = new SEC_Action();
        public string Add(DTO_Add action)
        {
            try
            {
                action.Action_Name.Trim();

                if (!act.IsAlreadyExists(action))
                {
                    bool result = act.Add(action);
                    if (result)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Error While Adding Feature!! Try Again later";
                    }
                }
                else
                {
                    return "Same Name or Feature URL Already Exists!!";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string Update(DTO_Update action)
        {
            try
            {
                bool result = act.Update(action);

                if (result)
                {
                    return "Success";
                }
                else
                {
                    return "Something Went wrong while Updating Action.!!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Delete(DTO_Delete action)
        {
            try
            {
                bool result = act.Delete(action);
                if (result)
                {
                    return "Success";
                }
                else
                {
                    return "Error while Deleting Feature!! Try again letter";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                return act.Search(searchTerm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetById(int id)
        {
            try
            {
                return act.GetById(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public List<DTO_GetAll> GetAll()
        {
            try
            {
                return act.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> GetActionForFeature(int Feature_Id)
        {
            try
            {
                return act.GetActionForFeature(Feature_Id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
