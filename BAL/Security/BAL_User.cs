﻿using System;
using System.Collections;
using System.Collections.Generic;
using DAL.Security;
using DTO;
using DTO.DTO_User;

namespace BAL.Security
{
    public class BAL_User: BAL_Interface<DTO_Add, DTO_Update, DTO_Delete, DTO_GetAll>
    {
        SEC_User user = new SEC_User();
        SEC_Role role = new SEC_Role();
        SEC_Status status = new SEC_Status();

        public bool Login(DTO_Login model)
        {
            try
            {
                return user.Login(model);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string Delete(DTO_Delete UserData)
        {
            try
            {
                if (user.Delete(UserData))
                {
                    return "Success";
                }
                else
                {
                    return "Error While deleting User Try again later!!!";
                }
            }
            catch (Exception ex)
            {
                return "Error Message: " + ex.Message.ToString();
            }
        }

        public DTO_GetAll GetById(int UserId)
        {
            try
            {
                DTO_GetAll UserData = user.GetById(UserId);
                if (!string.IsNullOrEmpty(UserData.EncryptionKey))
                    UserData.Password = BAL_PasswordEncryption.Decrypt(UserData.Password, UserData.EncryptionKey);
                return UserData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetByEmailOrUserName(string EmailOrUserName)
        {
            try
            {
                DTO_GetAll UserData = user.GetByEmailOrUserName(EmailOrUserName);
                return UserData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetEncryptionKey(string UsernameOrEmail)
        {
            try
            {
                return user.GetEncryptionKey(UsernameOrEmail);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool IsEmailAlreadyExist(string email)
        {
            try
            {
                return user.IsEmailAlreadyExist(email);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsUserNameAlreadyExist(string username)
        {
            try
            {
                return user.IsUserNameAlreadyExist(username);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string Add(DTO_Add data)
        {
            try
            {
                if (IsEmailAlreadyExist(data.Email))
                {
                    return "Email Already Exist!!!";
                }
                else if (IsUserNameAlreadyExist(data.UserName))
                {
                    return "Username Already Exist!!";
                }
                else
                {
                    if (data.Role_Id == 0)
                    {
                        data.Role_Id = role.GetId("user");
                    }
                    if (data.Status_Id == 0)
                    {
                        data.Status_Id = status.GetId("active");
                    }
                    data.EncryptionKey = BAL_PasswordEncryption.GenerateKey();
                    data.Password = BAL_PasswordEncryption.Encrypt(data.Password, data.EncryptionKey);

                    if (user.Add(data))
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Something went wrong while registering User!!!";
                    }
                }
            }
            catch (Exception ex)
            {
                return "Error Message: " + ex.Message.ToString();
            }
        }

        public List<DTO_GetAll> GetAll()
        {
            try
            {
                return user.GetAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(DTO_Update UserData)
        {
            try
            {
                UserData.EncryptionKey = BAL_PasswordEncryption.GenerateKey();

                UserData.Password = BAL_PasswordEncryption.Encrypt(UserData.Password, UserData.EncryptionKey);

                if (user.Update(UserData))
                {
                    return "Success";
                }
                else
                {
                    return "Data not updated!!!";
                }

            }
            catch (Exception ex)
            {
                return "Error Message: " + ex.Message.ToString();
            }
        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                return user.Search(searchTerm);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
