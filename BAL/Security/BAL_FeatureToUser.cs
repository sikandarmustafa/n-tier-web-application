﻿using System;
using System.Collections.Generic;
using DTO;
using DTO.DTO_Feature;
using DAL.Security;
using System.Data.SqlClient;
using System.Data;
using DAL;

namespace BAL.Security
{
    public class BAL_FeatureToUser
    {
        SEC_FeatureToUser FeatureToUser = new SEC_FeatureToUser();
        public string Add(DTO_FeatureToUser FU)
        {
            try
            {
                if (FU.AF_Id > 0 && FU.id > 0)
                {
                    if (FeatureToUser.Add(FU))
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Something Went wrong while adding Feature To Role!!";
                    }
                }
                else
                {
                    return "Invalid Selection!!!";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Delete(DTO_FeatureToUser FU)
        {
            try
            {
                if (FU.AF_Id > 0 && FU.id > 0)
                {
                    if (FeatureToUser.Delete(FU))
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Something Went wrong while Deleting !!";
                    }
                }
                else
                {
                    return "Invalid Selection!!!";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string MarkAll(int userId)
        {
            try
            {
                if (FeatureToUser.MarkAll(userId))
                {
                    return "Success";
                }
                else
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UnMarkAll(int userId)
        {
            try
            {
                if (FeatureToUser.UnMarkAll(userId))
                {
                    return "Success";
                }
                else
                {
                    return "Error";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> GetFeaturesForSideBarForSpecificUser(int id)
        {
            try
            {
                return FeatureToUser.GetFeaturesForSideBarForSpecificUser(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<DTO_GetAll> GetFeaturesAndActionsForAllUsers()
        {
            try
            {
                return FeatureToUser.GetFeaturesAndActionsForAllUsers();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> GetFeaturesAndActionsForSpecificUser(int id)
        {
            try
            {
                return FeatureToUser.GetFeaturesAndActionsForSpecificUser(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsAlreadyExist(DTO_FeatureToUser FU)
        {
            try
            {
                return FeatureToUser.IsAlreadyExist(FU);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
