USE [master]
GO
/****** Object:  Database [Inspinia]    Script Date: 01/07/2020 17:50:27 ******/
CREATE DATABASE [Inspinia] ON  PRIMARY 
( NAME = N'Inspinia', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Inspinia.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Inspinia_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Inspinia_log.LDF' , SIZE = 768KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Inspinia] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Inspinia].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Inspinia] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Inspinia] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Inspinia] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Inspinia] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Inspinia] SET ARITHABORT OFF
GO
ALTER DATABASE [Inspinia] SET AUTO_CLOSE ON
GO
ALTER DATABASE [Inspinia] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Inspinia] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Inspinia] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Inspinia] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Inspinia] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Inspinia] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Inspinia] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Inspinia] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Inspinia] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Inspinia] SET  ENABLE_BROKER
GO
ALTER DATABASE [Inspinia] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Inspinia] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Inspinia] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Inspinia] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Inspinia] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Inspinia] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Inspinia] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Inspinia] SET  READ_WRITE
GO
ALTER DATABASE [Inspinia] SET RECOVERY SIMPLE
GO
ALTER DATABASE [Inspinia] SET  MULTI_USER
GO
ALTER DATABASE [Inspinia] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Inspinia] SET DB_CHAINING OFF
GO
USE [Inspinia]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 01/07/2020 17:50:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[Status_Id] [int] IDENTITY(1,1) NOT NULL,
	[Status_Title] [varchar](20) NOT NULL,
	[InsertedBy] [int] NULL,
	[InsertedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK__Status__5190094C31B762FC] PRIMARY KEY CLUSTERED 
(
	[Status_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 01/07/2020 17:50:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](15) NOT NULL,
	[LastName] [varchar](15) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[UserName] [varchar](20) NOT NULL,
	[Role_Id] [int] NOT NULL,
	[Status_id] [int] NULL,
	[InsertedBy] [int] NULL,
	[InsertedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[EncryptionKey] [nvarchar](max) NULL,
 CONSTRAINT [PK__user_tbl__3213E83F7F60ED59] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SEC_UpdateUser]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_UpdateUser] @id INT = NULL
	,@FirstName VARCHAR(25) = NULL
	,@LastName VARCHAR(25) = NULL
	,@Email VARCHAR(25) = NULL
	,@Address VARCHAR(200) = NULL
	,@Password VARCHAR(25) = NULL
	,@UserName VARCHAR(25) = NULL
	,@Role_Id INT = NULL
	,@Status_Id INT = NULL
	,@UpdatedBy INT = NULL
	,@EncryptionKey VARCHAR(MAX) = NULL
AS
DECLARE @Value BIT

SET @Value = 1

BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.Users
	SET FirstName = @FirstName
		,LastName = @LastName
		,Email = @Email
		,[Address] = @Address
		,[Password] = @Password
		,Role_Id = @Role_Id
		,Status_id = @Status_Id
		,UpdatedBy = @UpdatedBy
		,UpdatedOn = GETDATE()
		,EncryptionKey =@EncryptionKey
	WHERE id = @id;

	RETURN @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_UpdateStatus]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_UpdateStatus] @Status_Id INT = NULL
	,@Status_Title VARCHAR(60) = NULL
	,@UpdatedBy INT = NULL
AS
DECLARE @Value INT

SET @Value = 1;

BEGIN
	SET NOCOUNT ON;

	UPDATE [Status]
	SET Status_Title = @Status_Title
		,UpdatedBy = @UpdatedBy
		,UpdatedOn = GETDATE()
	WHERE Status_Id = @Status_Id;

	RETURN @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_SearchUser]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_SearchUser] @SearcItem VARCHAR(60) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Users.id
		,Users.FirstName
		,Users.LastName
		,Users.Email
		,Users.UserName
		,Users.[Address]
		,Users.Role_Id
		,Users.Status_id
	FROM Users
	WHERE (FirstName LIKE '%' + @SearcItem + '%')
		OR (LastName LIKE '%' + @SearcItem + '%')
		OR (UserName LIKE '%' + @SearcItem + '%')
	ORDER BY Users.id;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_SearchStatus]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_SearchStatus] @SearchItem VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Status_Id
		,Status_Title
	FROM [Status]
	WHERE (Status_Id LIKE '%' + @SearchItem + '%')
		OR (Status_Title LIKE '%' + @SearchItem + '%');
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_RegisterUser]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_RegisterUser] @FirstName VARCHAR(15) = NULL
	,@LastName VARCHAR(15) = NULL
	,@Email VARCHAR(50) = NULL
	,@Password VARCHAR(50) = NULL
	,@Address VARCHAR(200) = NULL
	,@UserName VARCHAR(20) = NULL
	,@Role_Id INT = NULL
	,@Status_Id INT = NULL
	,@InsertedBy INT = NULL
	,@EncryptionKey VARCHAR(MAX) = NULL
AS
DECLARE @RetValue INT

BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.Users (
		FirstName
		,LastName
		,Email
		,[Password]
		,[Address]
		,UserName
		,Role_Id
		,Status_id
		,InsertedBy
		,InsertedOn
		,EncryptionKey
		)
	VALUES (
		@FirstName
		,@LastName
		,@Email
		,@Password
		,@Address
		,@UserName
		,@Role_Id
		,@Status_Id
		,@InsertedBy
		,GETDATE()
		,@EncryptionKey
		);

	SET @RetValue = SCOPE_IDENTITY();

	RETURN @RetValue;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_Login]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_Login] @UsernameOrEmail VARCHAR(80) = NULL
	,@Password VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT id
	FROM [dbo].Users
	WHERE (
			(
				Users.Email = @UsernameOrEmail
				OR Users.UserName = @UsernameOrEmail
				)
			AND Users.[Password] = @Password
			)
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetUserByUserName]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetUserByUserName] @UserName VARCHAR(60)
										
AS
BEGIN
	SET NOCOUNT ON;
	
				SELECT *
				FROM Users
				WHERE UserName = @UserName
			
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetUserById]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_GetUserById]
	-- Add the parameters for the stored procedure here
	@User_id INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

		SELECT *
		FROM dbo.Users
		WHERE id = @User_id

END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetUserByEmailOrUserName]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetUserByEmailOrUserName] @UserNameOrEmail VARCHAR(60)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT id
		,FirstName
		,LastName
		,Email
		,[Address]
		,UserName
		,Role_Id
		,Status_id
		,EncryptionKey
	FROM Users
	WHERE UserName = @UserNameOrEmail
		OR Email = @UserNameOrEmail;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetUserByEmail]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetUserByEmail] @Email VARCHAR(60)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT id
		,FirstName
		,LastName
		,Email
		,[Address]
		,[Password]
		,UserName
		,Role_Id
		,Status_id
	FROM Users
	WHERE Email = @Email;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetStatusById]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetStatusById] @Status_Id INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Status_Id
		,Status_Title
	FROM [Status]
	WHERE Status_Id = @Status_Id;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetEncrytionKeyByEmailOrUserName]    Script Date: 01/07/2020 17:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetEncrytionKeyByEmailOrUserName] @UserNameOrEmail VARCHAR(60)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT id,
		EncryptionKey
	FROM Users
	WHERE UserName = @UserNameOrEmail
		OR Email = @UserNameOrEmail;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetAllUsers]    Script Date: 01/07/2020 17:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sikandar,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_GetAllUsers]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT id
		,FirstName
		,LastName
		,Email
		,[Password]
		,[Address]
		,UserName
		,Role_Id
		,Status_id
	FROM dbo.[Users];
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetAllStatus]    Script Date: 01/07/2020 17:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetAllStatus]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Status_Id
		,Status_Title
	FROM [Status];
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_DeleteUserById]    Script Date: 01/07/2020 17:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_DeleteUserById] @User_id INT = NULL
AS
DECLARE @Value BIT

SET @Value = 1

BEGIN
	SET NOCOUNT ON;

	DELETE
	FROM dbo.[Users]
	WHERE id = @User_id;

	RETURN @Value
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_DeleteStatus]    Script Date: 01/07/2020 17:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_DeleteStatus] @Status_Id Int = Null
AS

Declare @Value INT
set @Value = 1
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete from [Status] Where Status_Id = @Status_Id;
	
	return @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_AddStatus]    Script Date: 01/07/2020 17:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_AddStatus] @Status_Title VARCHAR(50) = NULL
	,@InsertedBy INT = NULL
AS
DECLARE @Value INT

BEGIN
	SET NOCOUNT ON;

	INSERT INTO [Status] (
		Status_Title
		,InsertedBy
		,InsertedOn
		)
	VALUES (
		@Status_Title
		,@InsertedBy
		,GETDATE()
		);

	SET @Value = SCOPE_IDENTITY();

	RETURN @Value;
END
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 01/07/2020 17:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[Role_Id] [int] IDENTITY(1,1) NOT NULL,
	[Role_Title] [varchar](25) NOT NULL,
	[InsertedBy] [int] NULL,
	[InsertedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
	[Status_Id] [int] NULL,
 CONSTRAINT [PK__Roles__D80AB4BB0425A276] PRIMARY KEY CLUSTERED 
(
	[Role_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SEC_UpdateRole]    Script Date: 01/07/2020 17:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_UpdateRole] @Role_Id INT = NULL
	,@Role_Title VARCHAR(50) = NULL
	,@UpdatedBy INT = NULL
	,@Status_Id INT = NULL
AS
DECLARE @Value BIT

SET @Value = 1

BEGIN
	SET NOCOUNT ON;

	UPDATE [dbo].[Roles]
	SET [Roles].Role_Title = @Role_Title
		,UpdatedBy = @UpdatedBy
		,UpdatedOn = GETDATE()
		,Status_Id = @Status_Id
	WHERE [Roles].Role_Id = @Role_Id;

	RETURN @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_SearchRole]    Script Date: 01/07/2020 17:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_SearchRole] @SearchTerm VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Role_Id
		,Role_Title
		,Status_Id
	FROM [Roles]
	WHERE Role_Title LIKE '%' + @SearchTerm + '%';
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetRoleByTitle]    Script Date: 01/07/2020 17:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetRoleByTitle] @Role_Title VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Roles].Role_Id
		,[Roles].Role_Title
		,[Roles].Status_Id
	FROM Roles
	WHERE Role_Title = @Role_Title;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetRoleById]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetRoleById] @Role_id INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Roles].Role_Id
		,[Roles].Role_Title
		,[Roles].Status_Id
	FROM Roles
	WHERE Role_Id = @Role_id;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetAllRoles]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_GetAllRoles]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Role_Id,Role_Title,Status_Id from dbo.Roles;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_DeleteRole]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_DeleteRole] @Role_Id INT = NULL
AS
DECLARE @Value BIT

SET @Value = 1

BEGIN
	SET NOCOUNT ON;

	DELETE Roles
	WHERE Role_Id = @Role_Id;

	RETURN @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_AddRole]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_AddRole]
	@Role_Title VARCHAR(50) = NULL
	,@InsertedBy INT = NULL
	,@Status_Id INT = NULL
AS

DECLARE @Value BIT


BEGIN
	SET NOCOUNT ON;

	INSERT INTO [Roles] (Role_Title,InsertedBy,InsertedOn,Status_Id)
	VALUES (
		@Role_Title,@InsertedBy,GETDATE(),@Status_Id
		);
	
	SET @Value =SCOPE_IDENTITY();
	RETURN @Value;

END
GO
/****** Object:  Table [dbo].[FeatureToUser]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeatureToUser](
	[FU_Id] [int] IDENTITY(1,1) NOT NULL,
	[id] [int] NOT NULL,
	[AF_Id] [int] NOT NULL,
 CONSTRAINT [PK__FeatureT__F5619873236943A5] PRIMARY KEY CLUSTERED 
(
	[FU_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SEC_DeleteFeatureToUser]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_DeleteFeatureToUser] @id INT = NULL,@AF_Id INT = NULL
AS
DECLARE @Value INT

SET @Value = 1
BEGIN

	SET NOCOUNT ON;
	DELETE FeatureToUser WHERE id = @id AND AF_Id = @AF_Id;
	
	Return @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_AddFeatureToUser]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_AddFeatureToUser] @id INT = NULL
	,@AF_Id INT = NULL
AS
DECLARE @VALUE INT;

BEGIN
	SET NOCOUNT ON;

	INSERT INTO FeatureToUser (
		id
		,AF_Id
		)
	VALUES (
		@id
		,@AF_Id
		);

	SET @VALUE = SCOPE_IDENTITY();

	RETURN @VALUE;
END
GO
/****** Object:  Table [dbo].[FeatureToRole]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeatureToRole](
	[FR_Id] [int] IDENTITY(1,1) NOT NULL,
	[Role_id] [int] NOT NULL,
	[AF_Id] [int] NOT NULL,
 CONSTRAINT [PK__FeatureT__DC1C69DE151B244E] PRIMARY KEY CLUSTERED 
(
	[FR_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetFeatureToRoleByRoleId]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_GetFeatureToRoleByRoleId] @Role_Id INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	Select FR_Id,AF_Id from FeatureToRole where Role_id = @Role_Id;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_DeleteFeatureToRole]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_DeleteFeatureToRole] @Role_Id INT = NULL,@AF_Id INT = NULL
AS
DECLARE @Value INT

SET @Value = 1
BEGIN

	SET NOCOUNT ON;
	DELETE FeatureToRole WHERE Role_Id = @Role_Id AND AF_Id = @AF_Id;
	
	Return @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_AddFeatureToRole]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_AddFeatureToRole] @Role_Id INT = NULL
	,@AF_Id INT = NULL
AS
DECLARE @VALUE INT;

BEGIN
	SET NOCOUNT ON;

	INSERT INTO FeatureToRole (
		Role_Id
		,AF_Id
		)
	VALUES (
		@Role_Id
		,@AF_Id
		);

	SET @VALUE = SCOPE_IDENTITY();

	RETURN @VALUE;
END
GO
/****** Object:  Table [dbo].[Features]    Script Date: 01/07/2020 17:52:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Features](
	[Feature_Id] [int] IDENTITY(1,1) NOT NULL,
	[Feature_Name] [varchar](100) NOT NULL,
	[Feature_URL] [varchar](150) NOT NULL,
	[Feature_Icon] [varchar](50) NOT NULL,
	[Status_id] [int] NULL,
	[InsertedBy] [int] NULL,
	[InsertedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK__Features__F3BC02061A14E395] PRIMARY KEY CLUSTERED 
(
	[Feature_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SEC_UpdateFeature]    Script Date: 01/07/2020 17:52:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_UpdateFeature] @Feature_Id INT = NULL
	,@Feature_Name VARCHAR(60) = NULL
	,@Feature_URL VARCHAR(120) = NULL
	,@Feature_Icon VARCHAR(60) = NULL
	,@UpdatedBy INT = NULL
	,@Status_Id INT =NULL
AS
DECLARE @Value BIT

SET @Value = 1

BEGIN
	SET NOCOUNT ON;

	UPDATE Features
	SET Feature_Name = @Feature_Name
		,Feature_URL = @Feature_URL
		,Feature_Icon = @Feature_Icon
		,UpdatedBy = @UpdatedBy
		,UpdatedOn = GETDATE()
		,Status_id = @Status_Id
	WHERE Feature_Id = @Feature_Id;

	RETURN @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_SearchFeatures]    Script Date: 01/07/2020 17:52:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_SearchFeatures] @SearchItem VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Feature_Id
		,Feature_Name
		,Feature_URL
		,Feature_Icon
		,Status_id
	FROM Features
	WHERE (Feature_Name LIKE '%' + @SearchItem + '%')
		OR (Feature_Url LIKE '%' + @SearchItem + '%')
		OR (Feature_Icon LIKE '%' + @SearchItem + '%') 
		OR (Feature_Id LIKE '%' + @SearchItem + '%');
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_IsFeatureAlreadyExist]    Script Date: 01/07/2020 17:52:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_IsFeatureAlreadyExist] @Feature_Name varchar(100) = NULL
	,@Feature_Url varchar(100) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT Feature_Id from Features where Feature_Name = @Feature_Name OR Feature_URL = @Feature_Url;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetFeatureById]    Script Date: 01/07/2020 17:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetFeatureById] @Feature_Id INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select * from Features where Feature_id = @Feature_Id;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetAllFeatures]    Script Date: 01/07/2020 17:52:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetAllFeatures]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Feature_Id
		,Feature_Name
		,Feature_URL
		,Feature_Icon
		,Status_id
	FROM Features;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_DeleteFeature]    Script Date: 01/07/2020 17:52:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_DeleteFeature] @Feature_Id INT = NULL
AS
DECLARE @Value INT
Set @Value = 1
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE
	FROM Features
	WHERE Feature_Id = @Feature_Id;

	RETURN @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_AddFeature]    Script Date: 01/07/2020 17:52:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_AddFeature] @Feature_Name VARCHAR(60) = NULL
	,@Feature_Url VARCHAR(120) = NULL
	,@Feature_Icon VARCHAR(50) = NULL
	,@InsertedBy INT = NULL
	,@Status_Id INT = NULL
AS
DECLARE @Value INT

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Features (
		Feature_Name
		,Feature_URL
		,Feature_Icon
		,InsertedBy
		,InsertedOn
		,Status_id
		)
	VALUES (
		@Feature_Name
		,@Feature_Url
		,@Feature_Icon
		,@InsertedBy
		,GETDATE()
		,@Status_Id
		);

	SET @Value = SCOPE_IDENTITY();

	RETURN @Value;
END
GO
/****** Object:  Table [dbo].[ActionToFeature]    Script Date: 01/07/2020 17:52:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActionToFeature](
	[AF_Id] [int] IDENTITY(1,1) NOT NULL,
	[Feature_Id] [int] NOT NULL,
	[Action_Id] [int] NOT NULL,
 CONSTRAINT [PK__ActionTo__A6FED76B787EE5A0] PRIMARY KEY CLUSTERED 
(
	[AF_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SEC_IsActionToFeatureAlreadyExist]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_IsActionToFeatureAlreadyExist] @Feature_Id INT = NULL
	,@Action_Id INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT AF_Id from ActionToFeature where Feature_Id = @Feature_Id AND Action_Id = @Action_Id
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetFeaturesFromActionToFeature]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetFeaturesFromActionToFeature]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT AF.Feature_Id
		,F.Feature_Name
		,F.Feature_Icon
		,F.Feature_URL
	FROM ActionToFeature AS AF
	JOIN Features AS F ON AF.Feature_Id = F.Feature_Id
	ORDER BY F.Feature_Name;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetFeatureForSpecificUser]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_GetFeatureForSpecificUser] @id INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT DISTINCT AF.AF_Id,AF.Feature_Id
		,AF.Action_Id
	FROM FeatureToUser AS FU
	JOIN ActionToFeature AS AF ON FU.AF_Id = AF.AF_Id
	Where FU.id = @id;
	
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetFeatureForSpecificRole]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_GetFeatureForSpecificRole] @Role_Id INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT DISTINCT AF.AF_Id,AF.Feature_Id
		,AF.Action_Id
	FROM FeatureToRole AS FR
	JOIN ActionToFeature AS AF ON FR.AF_Id = AF.AF_Id
	Where FR.Role_Id = @Role_Id;
	
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetAllActionToFeature]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetAllActionToFeature] 
AS
BEGIN
	
	SET NOCOUNT ON;
	select AF_Id,Feature_Id,Action_Id from ActionToFeature;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_DeleteActionToFeature]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_DeleteActionToFeature] @Feature_Id INT = NULL
	,@Action_Id INT = NULL
AS
DECLARE @Value INT

SET @Value = 1

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE ActionToFeature
	WHERE Feature_Id = @Feature_Id
		AND Action_Id = @Action_Id;

	RETURN @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_AddActionToFeature]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_AddActionToFeature] @Feature_Id INT = NULL
	,@Action_Id INT = NULL
AS
DECLARE @Value INT

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO ActionToFeature
	VALUES (
		@Feature_Id
		,@Action_Id
		);

	SET @Value = SCOPE_IDENTITY();

	RETURN @Value;
END
GO
/****** Object:  Table [dbo].[Actions]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Actions](
	[Action_Id] [int] IDENTITY(1,1) NOT NULL,
	[Action_Name] [varchar](100) NOT NULL,
	[Status_id] [int] NULL,
	[InsertedBy] [int] NULL,
	[InsertedOn] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Action_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SEC_UpdateAction]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_UpdateAction] @Action_Id INT = NULL
	,@Action_Name VARCHAR(60) = NULL
	,@UpdatedBy INT = NULL
	,@Status_Id INT = NULL
AS
DECLARE @Value INT

SET @Value = 1;

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE Actions
	SET Action_Name = @Action_Name
		,UpdatedBy = @UpdatedBy
		,UpdatedOn = GETDATE()
		,Status_id = @Status_Id
	WHERE Action_Id = @Action_Id;

	RETURN @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_SearchActions]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_SearchActions] @SearchItem VARCHAR(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Action_Id
		,Action_Name
		,Status_id
	FROM Actions
	WHERE (Action_Id LIKE '%' + @SearchItem + '%')
		OR (Action_Name LIKE '%' + @SearchItem + '%');
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_IsActionAlreadyExist]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_IsActionAlreadyExist] @Action_Name varchar(100) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT Action_Id from Actions where Action_Name = @Action_Name;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetAllActions]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetAllActions]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Action_id
		,Action_Name,Status_id
	FROM Actions;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetActionsForFeatureForSpecificUser]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetActionsForFeatureForSpecificUser] @AF_Id INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		AF.Action_Id
		,A.Action_Name
	from ActionToFeature AS AF
	JOIN Actions AS A ON AF.Action_Id = A.Action_Id
	WHERE AF.AF_Id = @AF_Id;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetActionsForFeature]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetActionsForFeature] @Feature_Id INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT AF.AF_Id,AF.Action_Id
		,A.Action_Name
	FROM ActionToFeature AS AF
	JOIN Actions AS A ON AF.Action_Id = A.Action_Id
	WHERE AF.Feature_Id = @Feature_Id
	ORDER BY A.Action_Name;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_GetActionById]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_GetActionById] @Action_Id INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Action_Id
		,Action_Name
		,Status_id
	FROM Actions
	WHERE Action_Id = @Action_Id;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_DeleteAction]    Script Date: 01/07/2020 17:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SEC_DeleteAction] @Action_Id Int = Null
AS

Declare @Value INT
set @Value = 1
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Delete from Actions Where Action_Id = @Action_Id;
	
	return @Value;
END
GO
/****** Object:  StoredProcedure [dbo].[SEC_AddAction]    Script Date: 01/07/2020 17:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEC_AddAction] @Action_Name VARCHAR(50) = NULL
	,@InsertedBy INT = NULL
	,@Status_Id INT = NULL
AS
DECLARE @Value INT

BEGIN
	SET NOCOUNT ON;

	INSERT INTO Actions (
		Action_Name
		,InsertedBy
		,InsertedOn
		,Status_id
		)
	VALUES (
		@Action_Name
		,@InsertedBy
		,GETDATE()
		,@Status_Id
		);

	SET @Value = SCOPE_IDENTITY();

	RETURN @Value;
END
GO
/****** Object:  ForeignKey [FK__Status__Inserted__4F47C5E3]    Script Date: 01/07/2020 17:50:38 ******/
ALTER TABLE [dbo].[Status]  WITH CHECK ADD FOREIGN KEY([InsertedBy])
REFERENCES [dbo].[Users] ([id])
GO
/****** Object:  ForeignKey [FK__Status__UpdatedB__503BEA1C]    Script Date: 01/07/2020 17:50:38 ******/
ALTER TABLE [dbo].[Status]  WITH CHECK ADD FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[Users] ([id])
GO
/****** Object:  ForeignKey [FK__user__Role_Id__060DEAE8]    Script Date: 01/07/2020 17:50:38 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK__user__Role_Id__060DEAE8] FOREIGN KEY([Role_Id])
REFERENCES [dbo].[Roles] ([Role_Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK__user__Role_Id__060DEAE8]
GO
/****** Object:  ForeignKey [FK__Users__InsertedB__56E8E7AB]    Script Date: 01/07/2020 17:50:38 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([InsertedBy])
REFERENCES [dbo].[Users] ([id])
GO
/****** Object:  ForeignKey [FK__Users__Status_id__3587F3E0]    Script Date: 01/07/2020 17:50:38 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([Status_id])
REFERENCES [dbo].[Status] ([Status_Id])
GO
/****** Object:  ForeignKey [FK__Users__UpdatedBy__57DD0BE4]    Script Date: 01/07/2020 17:50:38 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[Users] ([id])
GO
/****** Object:  ForeignKey [FK__Roles__InsertedB__55009F39]    Script Date: 01/07/2020 17:51:53 ******/
ALTER TABLE [dbo].[Roles]  WITH CHECK ADD FOREIGN KEY([InsertedBy])
REFERENCES [dbo].[Users] ([id])
GO
/****** Object:  ForeignKey [FK__Roles__Status_Id__58D1301D]    Script Date: 01/07/2020 17:51:53 ******/
ALTER TABLE [dbo].[Roles]  WITH CHECK ADD FOREIGN KEY([Status_Id])
REFERENCES [dbo].[Status] ([Status_Id])
GO
/****** Object:  ForeignKey [FK__Roles__UpdatedBy__55F4C372]    Script Date: 01/07/2020 17:51:53 ******/
ALTER TABLE [dbo].[Roles]  WITH CHECK ADD FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[Users] ([id])
GO
/****** Object:  ForeignKey [FK__FeatureTo__AF_Id__2645B050]    Script Date: 01/07/2020 17:52:18 ******/
ALTER TABLE [dbo].[FeatureToUser]  WITH CHECK ADD  CONSTRAINT [FK__FeatureTo__AF_Id__2645B050] FOREIGN KEY([AF_Id])
REFERENCES [dbo].[ActionToFeature] ([AF_Id])
GO
ALTER TABLE [dbo].[FeatureToUser] CHECK CONSTRAINT [FK__FeatureTo__AF_Id__2645B050]
GO
/****** Object:  ForeignKey [FK__FeatureToUse__id__25518C17]    Script Date: 01/07/2020 17:52:18 ******/
ALTER TABLE [dbo].[FeatureToUser]  WITH CHECK ADD  CONSTRAINT [FK__FeatureToUse__id__25518C17] FOREIGN KEY([id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[FeatureToUser] CHECK CONSTRAINT [FK__FeatureToUse__id__25518C17]
GO
/****** Object:  ForeignKey [FK__FeatureTo__AF_Id__17F790F9]    Script Date: 01/07/2020 17:52:18 ******/
ALTER TABLE [dbo].[FeatureToRole]  WITH CHECK ADD  CONSTRAINT [FK__FeatureTo__AF_Id__17F790F9] FOREIGN KEY([AF_Id])
REFERENCES [dbo].[ActionToFeature] ([AF_Id])
GO
ALTER TABLE [dbo].[FeatureToRole] CHECK CONSTRAINT [FK__FeatureTo__AF_Id__17F790F9]
GO
/****** Object:  ForeignKey [FK__FeatureTo__Role___17036CC0]    Script Date: 01/07/2020 17:52:18 ******/
ALTER TABLE [dbo].[FeatureToRole]  WITH CHECK ADD  CONSTRAINT [FK__FeatureTo__Role___17036CC0] FOREIGN KEY([Role_id])
REFERENCES [dbo].[Roles] ([Role_Id])
GO
ALTER TABLE [dbo].[FeatureToRole] CHECK CONSTRAINT [FK__FeatureTo__Role___17036CC0]
GO
/****** Object:  ForeignKey [FK__Features__Insert__531856C7]    Script Date: 01/07/2020 17:52:18 ******/
ALTER TABLE [dbo].[Features]  WITH CHECK ADD FOREIGN KEY([InsertedBy])
REFERENCES [dbo].[Users] ([id])
GO
/****** Object:  ForeignKey [FK__Features__Status__3864608B]    Script Date: 01/07/2020 17:52:18 ******/
ALTER TABLE [dbo].[Features]  WITH CHECK ADD FOREIGN KEY([Status_id])
REFERENCES [dbo].[Status] ([Status_Id])
GO
/****** Object:  ForeignKey [FK__Features__Update__540C7B00]    Script Date: 01/07/2020 17:52:19 ******/
ALTER TABLE [dbo].[Features]  WITH CHECK ADD FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[Users] ([id])
GO
/****** Object:  ForeignKey [FK__ActionToF__Actio__7B5B524B]    Script Date: 01/07/2020 17:52:23 ******/
ALTER TABLE [dbo].[ActionToFeature]  WITH CHECK ADD  CONSTRAINT [FK__ActionToF__Actio__7B5B524B] FOREIGN KEY([Action_Id])
REFERENCES [dbo].[Actions] ([Action_Id])
GO
ALTER TABLE [dbo].[ActionToFeature] CHECK CONSTRAINT [FK__ActionToF__Actio__7B5B524B]
GO
/****** Object:  ForeignKey [FK__ActionToF__Featu__7A672E12]    Script Date: 01/07/2020 17:52:23 ******/
ALTER TABLE [dbo].[ActionToFeature]  WITH CHECK ADD  CONSTRAINT [FK__ActionToF__Featu__7A672E12] FOREIGN KEY([Feature_Id])
REFERENCES [dbo].[Features] ([Feature_Id])
GO
ALTER TABLE [dbo].[ActionToFeature] CHECK CONSTRAINT [FK__ActionToF__Featu__7A672E12]
GO
/****** Object:  ForeignKey [FK__Actions__Inserte__51300E55]    Script Date: 01/07/2020 17:52:24 ******/
ALTER TABLE [dbo].[Actions]  WITH CHECK ADD FOREIGN KEY([InsertedBy])
REFERENCES [dbo].[Users] ([id])
GO
/****** Object:  ForeignKey [FK__Actions__Status___37703C52]    Script Date: 01/07/2020 17:52:24 ******/
ALTER TABLE [dbo].[Actions]  WITH CHECK ADD FOREIGN KEY([Status_id])
REFERENCES [dbo].[Status] ([Status_Id])
GO
/****** Object:  ForeignKey [FK__Actions__Updated__5224328E]    Script Date: 01/07/2020 17:52:24 ******/
ALTER TABLE [dbo].[Actions]  WITH CHECK ADD FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[Users] ([id])
GO
