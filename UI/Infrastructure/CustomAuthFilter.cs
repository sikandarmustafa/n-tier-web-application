﻿using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;

namespace UI.Infrastructure
{
    public class CustomAuthFilter : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            if (filterContext.HttpContext.Session["UserData"] == null)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
            else
            {
                if (filterContext.HttpContext.Session["UserData"] == null)
                {
                    BAL.Security.BAL_User user = new BAL.Security.BAL_User();
                    DTO.DTO_User.DTO_GetAll UserData = user.GetByEmailOrUserName(filterContext.HttpContext.User.Identity.Name);
                    //Session["UserData"] = UserData;
                }
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            {
                //Redirecting the user to the Login View of Account Controller  
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                     { "controller", "Home" },
                     { "action", "Logout" }
                });

                //Session["abc"] = "";
            }
        }
    }
}