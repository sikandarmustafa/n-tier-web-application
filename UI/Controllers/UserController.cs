﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BAL.Security;
using DTO.DTO_User;

namespace UI.Controllers
{
    [Authorize,HandleError]
    public class UserController : Controller
    {
        BAL_Role role = new BAL_Role();
        BAL_Status status = new BAL_Status();
        BAL_User user = new BAL_User();


        // GET: User
        public ActionResult Index()
        {
            try
            {
                List<DTO.DTO_Role.DTO_GetAll> data = role.GetAll();
                ViewBag.RoleList = new SelectList(data, "Role_ID", "Role_Title");

                List<DTO.DTO_Status.DTO_GetAll> Stat = status.GetAll();
                ViewBag.StatusList = new SelectList(Stat, "Status_Id", "Status_Title");

                ViewBag.UserList = user.GetAll();
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult Display(int id)
        {
            try
            {
                DTO_GetAll UserModel = user.GetById(id);
                List<DTO.DTO_Role.DTO_GetAll> data = role.GetAll();
                ViewBag.RoleList = new SelectList(data, "Role_ID", "Role_Title", UserModel.Role_Id);
                return Json(UserModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet,AllowAnonymous]
        public ActionResult Add()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, AllowAnonymous]
        public ActionResult Add(DTO_Add AddData)
        {
            try
            {
                if (Request.IsAjaxRequest())
                {
                    if (!ModelState.IsValid)
                    {
                        return Json("Validate All Fields First!!!");
                    }
                    else
                    {
                        DTO_GetAll user1 = (DTO_GetAll)Session["UserData"];
                        AddData.InsertedBy = user1.id;
                        string Result = user.Add(AddData);
                        return Json(Result, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (!ModelState.IsValid)
                    {
                        return View(AddData);
                    }
                    else
                    {
                        AddData.InsertedBy = null;

                        string Result = user.Add(AddData);
                        switch (Result)
                        {
                            case "Success":
                                Session["successreg"] = "Success";
                                return View();
                            default:
                                //ModelState.AddModelError("", Result);
                                Session["successreg"] = Result;
                                return View();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Update(DTO_Update UserData)
        {
            try
            {
                DTO_GetAll user1 = (DTO_GetAll)Session["UserData"];
                UserData.UpdatedBy = (int)user1.id;
                string result = user.Update(UserData);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Delete(DTO_Delete UserData)
        {
            try
            {
                string result = user.Delete(UserData);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Search(string SearchTerm)
        {
            try
            {
                List<DTO_GetAll> result = user.Search(SearchTerm);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAll()
        {
            try
            {
                return Json(user.GetAll(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Dashboard()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}