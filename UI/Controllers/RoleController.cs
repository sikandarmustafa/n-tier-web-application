﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO.DTO_Role;
using BAL.Security;
using UI.Infrastructure;

namespace UI.Controllers
{   
    [CustomAuthFilter]
    public class RoleController : Controller
    {
        BAL_Role role = new BAL_Role();
        BAL_Status status = new BAL_Status();
        // GET: Role
        public ActionResult Index()
        {
            try
            {
                ViewBag.RoleList = role.GetAll();
                var Stat = status.GetAll();
                ViewBag.StatusList = new SelectList(Stat, "Status_Id", "Status_Title");
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Add(DTO_Add roledata)
        {
            try
            {
                DTO.DTO_User.DTO_GetAll user = (DTO.DTO_User.DTO_GetAll)Session["UserData"];
                roledata.InsertedBy = (int)user.id;
                string Result = role.Add(roledata);
                return Json(Result,JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult Display(int id)
        {
            try
            {
                return Json(role.GetById(id), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Delete(DTO_Delete RoleData)
        {
            try
            {
                return Json(role.Delete(RoleData), JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Update(DTO_Update RoleData)
        {
            try
            {
                DTO.DTO_User.DTO_GetAll user = (DTO.DTO_User.DTO_GetAll)Session["UserData"];
                RoleData.UpdatedBy = (int)user.id;
                string result = role.Update(RoleData);
                return Json(result,JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Search(string SearchTerm)
        {
            try
            {
                return Json(role.Search(SearchTerm), JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult GetAll()
        {
            try
            {
                return Json(role.GetAll(),JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}