﻿using BAL.Security;
using DTO;
using DTO.DTO_User;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace UI.Controllers
{
    public class HomeController : Controller
    {
        BAL_FeatureToUser FeatureToUser = new BAL_FeatureToUser();
        BAL_User user = new BAL_User();
        BAL_Status status = new BAL_Status();

        public ActionResult Index()
        {
            try
            {
                return RedirectToAction("Login");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            try
            {
                if (string.IsNullOrEmpty(returnUrl) && Request.UrlReferrer != null)
                    returnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);

                if (Url.IsLocalUrl(returnUrl) && !string.IsNullOrEmpty(returnUrl))
                {
                    ViewBag.ReturnURL = returnUrl;
                }
                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Login(DTO_Login LoginModel, string returnUrl)
        {
            try
            {
                string EncryptionKey = user.GetEncryptionKey(LoginModel.EmailOrUsername);

                if (EncryptionKey == "NotFound")
                {
                    ModelState.AddModelError("", "Account Not Exist. Create Account First!!!");
                    return View(LoginModel);
                }

                LoginModel.Password = BAL_PasswordEncryption.Encrypt(LoginModel.Password, EncryptionKey);
                if (user.Login(LoginModel))
                {
                    string decodedUrl = "";
                    if (!string.IsNullOrEmpty(returnUrl))
                        decodedUrl = Server.UrlDecode(returnUrl);

                    var UserData = user.GetByEmailOrUserName(LoginModel.EmailOrUsername);


                    Session["UserData"] = UserData;
                    DTO.DTO_Status.DTO_GetAll s = status.GetById(UserData.Status_Id);

                    if (s.Status_Title == "Inactive")
                    {
                        ModelState.AddModelError("", "Your Account Status is not Active!!!");
                        return View(LoginModel);
                    }

                    Session["FeaturesAndActions"] = FeatureToUser.GetFeaturesForSideBarForSpecificUser((int)UserData.id);


                    Session["AllowedActions"] = FeatureToUser.GetFeaturesAndActionsForSpecificUser((int)UserData.id);

                    FormsAuthentication.SetAuthCookie(LoginModel.EmailOrUsername, LoginModel.RememberMe);

                    if (Url.IsLocalUrl(decodedUrl))
                    {
                        return Redirect(decodedUrl);
                    }
                    else
                    {
                        return RedirectToAction("Dashboard", "User");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Email or Password Incorrect!!!");
                    return View(LoginModel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Logout()
        {
            try
            {
                Session.RemoveAll();
                Session.Clear();
                FormsAuthentication.SignOut();
                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}