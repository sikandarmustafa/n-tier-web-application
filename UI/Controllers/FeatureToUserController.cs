﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using DTO.DTO_User;
using BAL.Security;

namespace UI.Controllers
{
    [Authorize]
    public class FeatureToUserController : Controller
    {
        BAL_User user = new BAL_User();
        BAL_FeatureToUser FeatureToUser = new BAL_FeatureToUser();
        // GET: FeatureToUser

        public ActionResult Index()
        {
            try
            {
                List<DTO_GetAll> data = user.GetAll();
                ViewBag.UserList = new SelectList(data, "id", "Username");
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Add(DTO_FeatureToUser FU)
        {
            try
            {
                return Json(FeatureToUser.Add(FU), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public JsonResult Delete(DTO_FeatureToUser FU)
        {
            try
            {
                return Json(FeatureToUser.Delete(FU), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult FeaturesAndActionsForUser(int id = 0)
        {
            try
            {
                List<DTO.DTO_Feature.DTO_GetAll> result = new List<DTO.DTO_Feature.DTO_GetAll>();
                if (id == 0)
                {
                    result = FeatureToUser.GetFeaturesAndActionsForAllUsers();
                }
                else
                {
                    result = FeatureToUser.GetFeaturesAndActionsForSpecificUser(id);
                }
                return PartialView(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public JsonResult MarkAll(DTO_FeatureToUser User_id)
        {
            try
            {
                return Json(FeatureToUser.MarkAll(User_id.id), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public JsonResult UnMarkAll(DTO_FeatureToUser User_id)
        {
            try
            {
                return Json(FeatureToUser.UnMarkAll(User_id.id), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}