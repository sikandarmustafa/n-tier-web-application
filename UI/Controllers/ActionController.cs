﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DTO.DTO_Action;
using BAL.Security;

namespace UI.Controllers
{
    [Authorize]
    public class ActionController : Controller
    {
        BAL_Action act = new BAL_Action();
        BAL_Status status = new BAL_Status();

        // GET: Action
        public ActionResult Index()
        {
            try
            {
                ViewBag.ActionList = act.GetAll();
                var Stat = status.GetAll();
                ViewBag.StatusList = new SelectList(Stat, "Status_Id", "Status_Title");
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Add(DTO_Add action1)
        {
            try
            {
                DTO.DTO_User.DTO_GetAll user = (DTO.DTO_User.DTO_GetAll)Session["UserData"];
                action1.InsertedBy = (int)user.id;
                string result = act.Add(action1);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Delete(DTO_Delete action1)
        {
            try
            {
                
                string result = act.Delete(action1);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult Search(string SearchTerm)
        {
            try
            {
                
                List<DTO_GetAll> result = act.Search(SearchTerm);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult Update(DTO_Update action1)
        {
            try
            {
                
                DTO.DTO_User.DTO_GetAll action = (DTO.DTO_User.DTO_GetAll)Session["UserData"];
                action1.UpdatedBy = (int)action.id;
                string result = act.Update(action1);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult Display(int id)
        {
            try
            {
                return Json(act.GetById(id),JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetAll()
        {
            try
            {
                return Json(act.GetAll(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}