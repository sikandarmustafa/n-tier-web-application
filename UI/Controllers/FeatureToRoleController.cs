﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using BAL.Security;


namespace UI.Controllers
{
    [Authorize]
    public class FeatureToRoleController : Controller
    {
        // GET: FeatureToRole
        BAL_FeatureToRole FeatureToRole = new BAL_FeatureToRole();
        BAL_Role role = new BAL_Role();

        public ActionResult Index()
        {
            try
            {

                List<DTO.DTO_Role.DTO_GetAll> data = role.GetAll();
                ViewBag.RoleList = new SelectList(data, "Role_ID", "Role_Title");
                return View();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Add(DTO_FeatureToRole FR)
        {
            try
            {
                return Json(FeatureToRole.Add(FR), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Delete(DTO_FeatureToRole FR)
        {
            try
            {
                return Json(FeatureToRole.Delete(FR), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult FeaturesAndActionsForRole(int roleId = 0)
        {
            List<DTO.DTO_Feature.DTO_GetAll> result = new List<DTO.DTO_Feature.DTO_GetAll>();
            if (roleId == 0)
            {
                result = FeatureToRole.GetFeaturesAndActionsForAllRoles();
            }
            else
            {
                result = FeatureToRole.GetFeaturesAndActionsForSpecificRole(roleId);  
            }
            return PartialView(result);

        }

        [HttpPost]
        public JsonResult MarkAll(DTO_FeatureToRole RoleId)
        {
            try
            {
                return Json(FeatureToRole.MarkAll(RoleId.Role_Id), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public JsonResult UnMarkAll(DTO_FeatureToRole RoleId)
        {
            try
            {
                return Json(FeatureToRole.UnMarkAll(RoleId.Role_Id), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}