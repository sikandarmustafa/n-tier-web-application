﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO.DTO_Feature;
using BAL.Security;

namespace UI.Controllers
{
    [Authorize]
    public class FeatureController : Controller
    {

        BAL_Feature ftr = new BAL_Feature();
        BAL_Status status = new BAL_Status();

        public ActionResult Index()
        {
            try
            {
                ViewBag.FeaturesAndActions = (List<DTO_GetAll>)Session["FeaturesAndActions"];
                ViewBag.FeatureList = ftr.GetAll();
                var Stat = status.GetAll();
                ViewBag.StatusList = new SelectList(Stat, "Status_Id", "Status_Title");
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Add(DTO_Add feature)
        {
            try
            {
               
                DTO.DTO_User.DTO_GetAll user = (DTO.DTO_User.DTO_GetAll)Session["UserData"];
                feature.InsertedBy = (int)user.id;
                string result = ftr.Add(feature);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult Delete(DTO_Delete feature)
        {
            try
            {
                string result = ftr.Delete(feature);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult Update(DTO_Update feature)
        {
            try
            {
                DTO.DTO_User.DTO_GetAll user = (DTO.DTO_User.DTO_GetAll)Session["UserData"];
                feature.UpdatedBy = (int)user.id;
                string result = ftr.Update(feature);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Display(int id)
        {
            try
            {
                return Json(ftr.GetById(id), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult Search(string SearchTerm)
        {
            try
            {
                List<DTO_GetAll> result = ftr.Search(SearchTerm);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetAll()
        {
            try
            {
                List<DTO_GetAll> result = ftr.GetAll();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult SideBar()
        {
            try
            {
                List<DTO_GetAll> sidebar;
                if (Request.IsAuthenticated)
                {
                    sidebar = (List<DTO_GetAll>)Session["FeaturesAndActions"];
                }
                else
                {
                    BAL_Feature ftr = new BAL_Feature();

                    sidebar = ftr.GetAll();
                }
                return PartialView(sidebar);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}