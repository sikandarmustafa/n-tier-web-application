﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO.DTO_Status;
using BAL.Security;

namespace UI.Controllers
{ 
    [Authorize]
    public class StatusController : Controller
    {
        BAL_Status status = new BAL_Status();


        // GET: Status
        public ActionResult Index()
        {
            try
            {
                ViewBag.StatusList = status.GetAll();
                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Add(DTO_Add StatusData)
        {
            try
            {
                DTO.DTO_User.DTO_GetAll user = (DTO.DTO_User.DTO_GetAll)Session["UserData"];
                StatusData.InsertedBy = (int)user.id;
                string result = status.Add(StatusData);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Delete(DTO_Delete StatusData)
        {
            try
            {
                return Json(status.Delete(StatusData), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult Search(string SearchTerm)
        {
            try
            {
                return Json(status.Search(SearchTerm), JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Update(DTO_Update StatusData)
        {
            try
            {
                DTO.DTO_User.DTO_GetAll user = (DTO.DTO_User.DTO_GetAll)Session["UserData"];
                StatusData.UpdatedBy = (int)user.id;
                string result = status.Update(StatusData);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult Display(int id)
        {
            try
            {
                return Json(status.GetById(id), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetAll()
        {
            try
            {
                return Json(status.GetAll(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}