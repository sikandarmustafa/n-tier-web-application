﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using BAL.Security;

namespace UI.Controllers
{
    [Authorize]
    public class ActionToFeatureController : Controller
    {
        // GET: ActionToFeature
        BAL_ActionToFeature ActionToFeature = new BAL_ActionToFeature();
        public ActionResult Index()
        {
            try
            {
                return View(ActionToFeature.GetAll());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        [HttpPost]
        public JsonResult Add(DTO_ActionToFeature AF)
        {
            try
            {
                return Json(ActionToFeature.Add(AF), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult Delete(DTO_ActionToFeature AF)
        {
            try
            {
                return Json(ActionToFeature.Delete(AF), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("You cannot remove this permission!! it is assigned to other users. Remove it first from user Technical Message: " + ex.Message.ToString(), JsonRequestBehavior.AllowGet);

            }
        }



    }
}