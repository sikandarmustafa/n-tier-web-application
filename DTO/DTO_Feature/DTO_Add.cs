﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.DTO_Feature
{
    public class DTO_Add
    {
        public string Feature_Name { get; set; }
        public string Feature_Url { get; set; }
        public string Feature_Icon { get; set; }
        public int InsertedBy { get; set; }

        public int Status_Id { get; set; }

    }
}
