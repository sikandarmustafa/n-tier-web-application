﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.DTO_Feature
{
    public class DTO_Update
    {
        public int Feature_Id { get; set; }
        public string Feature_Name { get; set; }
        public string Feature_Url { get; set; }
        public string Feature_Icon { get; set; }
        public int UpdatedBy { get; set; }
        public int Status_Id { get; set; }

    }
}
