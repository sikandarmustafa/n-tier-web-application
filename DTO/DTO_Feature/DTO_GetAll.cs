﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO.DTO_Action;

namespace DTO.DTO_Feature
{
    public class DTO_GetAll
    {
        public int Feature_Id { get; set; }
        public string Feature_Name { get; set; }
        public string  Feature_Url { get; set; }
        public string  Feature_Icon { get; set; }
        public bool IsSelected { get; set; }
        public List<DTO.DTO_Action.DTO_GetAll> Actions { get; set; }
        public int Status_Id { get; set; }
        public int InsertedBy { get; set; }
        public DateTime InsertedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

    }
}
