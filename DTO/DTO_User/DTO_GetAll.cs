﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DTO.DTO_User
{
    public class DTO_GetAll
    {
        public int id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        public string Address { get; set; }
        [Required, RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage = "Username can only contain alphabets and numbers!!!")]
        public string UserName { get; set; }
        public int Role_Id { get; set; }
        public int InsertedBy { get; set; }
        public DateTime InsertedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int Status_Id { get; set; }

        public string EncryptionKey { get; set; }

    }
}
