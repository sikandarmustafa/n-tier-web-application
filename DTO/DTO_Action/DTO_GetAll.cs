﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.DTO_Action
{
    public class DTO_GetAll
    {
        public int Action_Id { get; set; }
        public string Action_Name { get; set; }
        public int AF_Id { get; set; }
        public bool IsSelected { get; set; }
        public int InsertedBy { get; set; }
        public Nullable<DateTime> InsertedOn { get; set; }
        public int UpdatedBy { get; set; }
        public Nullable<DateTime> UpdatedOn { get; set; }
        public int Status_Id { get; set; }
    }
}