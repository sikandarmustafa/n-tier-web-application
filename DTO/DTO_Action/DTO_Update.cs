﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.DTO_Action
{
    public class DTO_Update
    {
        public int Action_Id { get; set; }
        public string Action_Name { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public int Status_Id { get; set; }
    }
}
