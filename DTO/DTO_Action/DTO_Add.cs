﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.DTO_Action
{
    public class DTO_Add
    {
        public string Action_Name { get; set; }
        public int InsertedBy { get; set; }
        public int Status_Id { get; set; }
    }
}
