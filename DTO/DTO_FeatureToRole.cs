﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_FeatureToRole
    {
        public int FR_Id { get; set; }
        public int Role_Id { get; set; }
        public int AF_Id { get; set; }
    }
}
