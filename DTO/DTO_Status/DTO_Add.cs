﻿namespace DTO.DTO_Status
{
    public class DTO_Add
    {
        public string Status_Title { get; set; }

        public int InsertedBy { get; set; }
    }
}
