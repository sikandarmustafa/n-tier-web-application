﻿namespace DTO.DTO_Status
{
    public class DTO_Update
    {
        public int Status_Id { get; set; }
        public string Status_Title { get; set; }

        public int UpdatedBy { get; set; }

    }
}
