﻿using System;

namespace DTO.DTO_Status
{
    public class DTO_GetAll
    {
        public int Status_Id { get; set; }
        public string Status_Title { get; set; }
        public int InsertedBy { get; set; }
        public DateTime InsertedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<DateTime> UpdatedOn { get; set; }
    }
}
