﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_ActionToFeature
    {
        public int AF_Id { get; set; }
        public int Feature_Id { get; set; }
        public int Action_Id { get; set; }
    }
}
