﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.DTO_Role
{
    public class DTO_Update
    {
        public int Role_Id { get; set; }
        public string Role_Title{ get; set; }
        public int UpdatedBy{ get; set; }
        public int Status_Id { get; set; }
    }
}
