﻿namespace DTO.DTO_Role
{
    public class DTO_Add
    {
        public string Role_Title { get; set; }

        public int InsertedBy { get; set; }

        public int Status_Id { get; set; }

    }
}
