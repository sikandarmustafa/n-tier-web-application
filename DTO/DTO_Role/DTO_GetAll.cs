﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DTO.DTO_Role
{
    public class DTO_GetAll
    {
        public int Role_Id { get; set; }
        [Required,MaxLength(20,ErrorMessage ="Role Name length shuold be less than 20")]
        public string Role_Title { get; set; }

        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int InsertedBy { get; set; }
        public DateTime InsertedOn { get; set; }
        public int Status_Id { get; set; }

    }
}
