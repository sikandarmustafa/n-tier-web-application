﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public sealed class DAL_DbConnection
    {
        public static string GetConnection()
        {
            try
            {
                return ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
