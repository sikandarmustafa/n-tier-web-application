﻿using DTO.DTO_Status;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DAL.Security
{
    public class SEC_Status : SEC_Interface<DTO_Add, DTO_Update, DTO_Delete, DTO_GetAll>
    {

        string CS = DAL_DbConnection.GetConnection();

        public bool Add(DTO_Add status)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_AddStatus", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Status_Title", status.Status_Title);
                    cmd.Parameters.AddWithValue("@InsertedBy", status.InsertedBy);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(DTO_Update status)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_UpdateStatus", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Status_Id", status.Status_Id);
                    cmd.Parameters.AddWithValue("@Status_Title", status.Status_Title);
                    cmd.Parameters.AddWithValue("@UpdatedBy", status.UpdatedBy);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public bool Delete(DTO_Delete status_Id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_DeleteStatus", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Status_Id", status_Id.Status_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)searchTerm };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_SearchStatus", array);

                    List<DTO_GetAll> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO.DTO_Status.DTO_GetAll
                        {
                            Status_Id = (int)dataRow.Field<int>("Status_Id"),
                            Status_Title = dataRow.Field<string>("Status_Title"),
                        }).ToList();

                    return result;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetById(int id)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetStatusById", array);

                    DataRow row = ds.Tables[0].Rows[0];

                    DTO_GetAll result = new DTO.DTO_Status.DTO_GetAll()
                    {
                        Status_Id = (int)row["Status_Id"],
                        Status_Title = row["Status_Title"].ToString()
                    };
                    return result;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<DTO_GetAll> GetAll()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetAllStatus", null);
                    List<DTO_GetAll> result = ds.Tables[0].AsEnumerable().Select(dataRow => new DTO.DTO_Status.DTO_GetAll
                    {
                        Status_Id = dataRow.Field<int>("Status_Id"),
                        Status_Title = dataRow.Field<String>("Status_Title")
                    }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsAlreadyExists(DTO_Add status)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    string Query = string.Format("select Status_Id from Status where Status_Title = '{0}'", status.Status_Title);
                    DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Query);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int GetId(string status)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)status };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetStatusByTitle", array);
                    DataRow row = ds.Tables[0].Rows[0];

                    return (int)row["Status_Id"];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
