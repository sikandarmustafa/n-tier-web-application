﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DTO.DTO_Feature;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace DAL.Security
{
    public class SEC_FeatureToRole
    {
        string CS = DAL_DbConnection.GetConnection();

        public bool Add(DTO_FeatureToRole FR)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_AddFeatureToRole", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@AF_Id", FR.AF_Id);
                    cmd.Parameters.AddWithValue("@Role_Id", FR.Role_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Delete(DTO_FeatureToRole FR)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_DeleteFeatureToRole", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@AF_Id", FR.AF_Id);
                    cmd.Parameters.AddWithValue("@Role_Id", FR.Role_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //Roles
        public List<DTO_GetAll> GetFeaturesAndActionsForAllRoles()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    SEC_Action act = new SEC_Action();
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetFeaturesFromActionToFeature", null);
                    List<DTO_GetAll> result = ds.Tables[0].AsEnumerable().Select(dataRow => new DTO_GetAll
                    {
                        Feature_Id = dataRow.Field<int>("Feature_Id"),
                        Feature_Name = dataRow.Field<String>("Feature_Name"),
                        Feature_Url = dataRow.Field<String>("Feature_URL"),
                        Feature_Icon = dataRow.Field<String>("Feature_Icon"),
                        IsSelected = false,
                        Actions = act.GetActionForFeature(dataRow.Field<int>("Feature_Id"))
                    }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> GetFeaturesAndActionsForSpecificRole(int Role_Id)
        {
            List<DTO_GetAll> result = GetFeaturesAndActionsForAllRoles();

            using (SqlConnection conn = new SqlConnection(CS))
            {
                object[] array = new[] { (object)Role_Id };
                DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetFeatureForSpecificRole", array);
                List<DataRow> features = ds.Tables[0].AsEnumerable().ToList();

                foreach (DTO_GetAll item in result)
                {
                    if (features.Any(x => x.Field<int>("Feature_Id") == item.Feature_Id))
                    {
                        if (!item.IsSelected)
                        {
                            item.IsSelected = true;
                        }

                        foreach (DTO.DTO_Action.DTO_GetAll action in item.Actions)
                        {
                            if (features.Any(x => x.Field<int>("Action_Id") == action.Action_Id && x.Field<int>("Feature_Id") == item.Feature_Id))
                            {
                                if (!action.IsSelected)
                                {
                                    action.IsSelected = true;
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool MarkAll(int Role_Id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetAllActionToFeature", null);
                    List<DTO_ActionToFeature> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO_ActionToFeature
                        {
                            Feature_Id = dataRow.Field<int>("Feature_Id"),
                            AF_Id = dataRow.Field<int>("AF_Id"),
                            Action_Id = dataRow.Field<int>("Action_Id"),
                        }).ToList();

                    DTO_FeatureToRole FeatureToRole = new DTO_FeatureToRole();
                    FeatureToRole.Role_Id = Role_Id;
                    foreach (var item in result)
                    {
                        FeatureToRole.AF_Id = item.AF_Id;
                        if (!IsAlreadyExist(FeatureToRole))
                        {
                            SqlCommand cmd = new SqlCommand("SEC_AddFeatureToRole", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@Role_Id", Role_Id);
                            cmd.Parameters.AddWithValue("@AF_Id", item.AF_Id);

                            SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                            Value.Direction = ParameterDirection.ReturnValue;

                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UnMarkAll(int Role_Id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_DeleteAllFeatureToRole", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Role_Id", Role_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsAlreadyExist(DTO_FeatureToRole FR)
        {
            try
            {

                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)FR.Role_Id, (object)FR.AF_Id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_IsFeatureToRoleAlreadyExist", array);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
