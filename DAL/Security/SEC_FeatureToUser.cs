﻿using System;
using System.Collections.Generic;
using System.Linq;
using DTO.DTO_Feature;
using DTO;
using System.Data.SqlClient;
using System.Data;

namespace DAL.Security
{
    public class SEC_FeatureToUser
    {
        string CS = DAL_DbConnection.GetConnection();
        SEC_Action act = new SEC_Action();
        SEC_ActionToFeature AF = new SEC_ActionToFeature();
        public bool Add(DTO_FeatureToUser FU)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_AddFeatureToUser", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", FU.id);
                    cmd.Parameters.AddWithValue("@AF_Id", FU.AF_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Delete(DTO_FeatureToUser FU)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_DeleteFeatureToUser", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@AF_Id", FU.AF_Id);
                    cmd.Parameters.AddWithValue("@id", FU.id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<DTO_GetAll> GetFeaturesForSideBarForSpecificUser(int id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetFeatureForSpecificUser", array);

                    List<DTO_ActionToFeature> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO_ActionToFeature
                        {
                            Feature_Id = dataRow.Field<int>("Feature_Id"),
                            AF_Id = dataRow.Field<int>("AF_Id"),
                            Action_Id = dataRow.Field<int>("Action_Id"),
                        }).ToList();


                    result = result.GroupBy(x => x.Feature_Id).Select(x => x.First()).ToList();

                    List<DTO_GetAll> FeatureList = new List<DTO_GetAll>();
                    foreach (DTO_ActionToFeature item in result)
                    {
                        object[] array1 = new[] { (object)item.Feature_Id };
                        DataSet ds1 = SqlHelper.ExecuteDataset(conn, "SEC_GetFeatureById", array1);

                        DataRow row = ds1.Tables[0].Rows[0];

                        DTO_GetAll feature = new DTO_GetAll()
                        {
                            Feature_Id = (int)row["Feature_Id"],
                            Feature_Name = row["Feature_Name"].ToString(),
                            Feature_Url = row["Feature_Url"].ToString(),
                            Feature_Icon = row["Feature_Icon"].ToString(),
                            Actions = act.GetActionForFeature((int)row["Feature_Id"])
                        };
                        FeatureList.Add(feature);
                    }
                    return FeatureList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Users
        public List<DTO_GetAll> GetFeaturesAndActionsForAllUsers()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetFeaturesFromActionToFeature", null);
                    List<DTO_GetAll> result = ds.Tables[0].AsEnumerable().Select(dataRow => new DTO_GetAll
                    {
                        Feature_Id = dataRow.Field<int>("Feature_Id"),
                        Feature_Name = dataRow.Field<String>("Feature_Name"),
                        Feature_Url = dataRow.Field<String>("Feature_URL"),
                        Feature_Icon = dataRow.Field<String>("Feature_Icon"),
                        IsSelected = false,
                        Actions = act.GetActionForFeature(dataRow.Field<int>("Feature_Id"))
                    }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> GetFeaturesAndActionsForSpecificUser(int id)
        {
            try
            {
                List<DTO_GetAll> result = GetFeaturesAndActionsForAllUsers();

                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetFeatureForSpecificUser", array);
                    List<DataRow> features = ds.Tables[0].AsEnumerable().ToList();

                    foreach (DTO_GetAll item in result)
                    {
                        if (features.Any(x => x.Field<int>("Feature_Id") == item.Feature_Id))
                        {
                            if (!item.IsSelected)
                            {
                                item.IsSelected = true;
                            }

                            foreach (var action in item.Actions)
                            {
                                if (features.Any(x => x.Field<int>("Action_Id") == action.Action_Id && x.Field<int>("Feature_Id") == item.Feature_Id))
                                {
                                    if (!action.IsSelected)
                                    {
                                        action.IsSelected = true;
                                    }
                                }
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool MarkAll(int UserId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetAllActionToFeature", null);
                    List<DTO_ActionToFeature> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO_ActionToFeature
                        {
                            Feature_Id = dataRow.Field<int>("Feature_Id"),
                            AF_Id = dataRow.Field<int>("AF_Id"),
                            Action_Id = dataRow.Field<int>("Action_Id"),
                        }).ToList();

                    DTO_FeatureToUser FeatureToUser = new DTO_FeatureToUser();
                    FeatureToUser.id = UserId;
                    foreach (var item in result)
                    {
                        FeatureToUser.AF_Id = item.AF_Id;
                        if (!IsAlreadyExist(FeatureToUser))
                        {
                            SqlCommand cmd = new SqlCommand("SEC_AddFeatureToUser", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@id", UserId);
                            cmd.Parameters.AddWithValue("@AF_Id", item.AF_Id);

                            SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                            Value.Direction = ParameterDirection.ReturnValue;


                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool UnMarkAll(int UserId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_DeleteAllFeatureToUser", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", UserId);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsAlreadyExist(DTO_FeatureToUser FU)
        {
            try
            {
                
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)FU.id, (object)FU.AF_Id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_IsFeatureToUserAlreadyExist", array);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
