﻿using System;
using System.Collections.Generic;
using System.Linq;
using DTO;
using DTO.DTO_Feature;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Security
{
    public class SEC_ActionToFeature
    {
        string CS = DAL_DbConnection.GetConnection();

        public List<DTO_GetAll> GetAll()
        {
            try
            {
                var result = GetAllFeaturesWithActions();

                using (SqlConnection conn = new SqlConnection(CS))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetAllActionToFeature", null);
                    var features = ds.Tables[0].AsEnumerable().ToList();

                    foreach (var item in result)
                    {
                        if (features.Any(x => x.Field<int>("Feature_Id") == item.Feature_Id))
                        {
                            if (!item.IsSelected)
                            {
                                item.IsSelected = true;
                            }

                            foreach (var action in item.Actions)
                            {
                                if (features.Any(x => (x.Field<int>("Action_Id") == action.Action_Id) && (x.Field<int>("Feature_Id") == item.Feature_Id)))
                                {
                                    if (!action.IsSelected)
                                    {
                                        action.IsSelected = true;
                                    }
                                }
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add(DTO_ActionToFeature FA)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_AddActionToFeature", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Feature_Id", FA.Feature_Id);
                    cmd.Parameters.AddWithValue("@Action_Id", FA.Action_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool IsAlreadyExist(DTO_ActionToFeature fA)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)fA.Feature_Id, (object)fA.Action_Id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_IsActionToFeatureAlreadyExist", array);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Delete(DTO_ActionToFeature FA)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_DeleteActionToFeature", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Feature_Id", FA.Feature_Id);
                    cmd.Parameters.AddWithValue("@Action_Id", FA.Action_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<DTO_GetAll> GetAllFeaturesWithActions()
        {
            try
            {
                SEC_Action act = new SEC_Action();
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    var ds = SqlHelper.ExecuteDataset(conn, "SEC_GetAllFeatures", null);
                    var result = ds.Tables[0].AsEnumerable().Select(dataRow => new DTO_GetAll
                    {
                        Feature_Id = dataRow.Field<int>("Feature_Id"),
                        Feature_Name = dataRow.Field<String>("Feature_Name"),
                        Feature_Url = dataRow.Field<String>("Feature_URL"),
                        Feature_Icon = dataRow.Field<String>("Feature_Icon"),
                        IsSelected = false,
                        Actions = act.GetAll()
                    }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
