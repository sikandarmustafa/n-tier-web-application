﻿using DTO;
using DTO.DTO_User;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DAL.Security
{
    public class SEC_User: SEC_Interface<DTO_Add, DTO_Update, DTO_Delete, DTO_GetAll>
    {
        string CS = DAL_DbConnection.GetConnection();

        public bool Login(DTO_Login model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)model.EmailOrUsername, (object)model.Password };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_Login", array);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetById(int userId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)userId };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetUserById", array);
                    DataTable result = ds.Tables[0];

                    DataRow row = result.Rows[0];

                    DTO_GetAll User = new DTO_GetAll()
                    {
                        id = (int)row["id"],
                        FirstName = row["FirstName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        Email = row["Email"].ToString(),
                        Address = row["Address"].ToString(),
                        Password = row["Password"].ToString(),
                        UserName = row["UserName"].ToString(),
                        Role_Id = (int)row["Role_Id"],
                        Status_Id = (int)row["Status_Id"],
                        EncryptionKey = row["EncryptionKey"].ToString()
                    };
                    return User;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetEncryptionKey(string UsernameOrEmail)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)UsernameOrEmail };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetEncrytionKeyByEmailOrUserName", array);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow row = ds.Tables[0].Rows[0];

                        var EncryptionKey = row["EncryptionKey"].ToString();
                        return EncryptionKey;
                    }
                    else
                    {
                        return "NotFound";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetByEmailOrUserName(string emailOrUserName)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)emailOrUserName };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetUserByEmailOrUserName", array);
                    DataRow row = ds.Tables[0].Rows[0];

                    DTO_GetAll User = new DTO_GetAll()
                    {
                        id = (int)row["id"],
                        FirstName = row["FirstName"].ToString(),
                        LastName = row["LastName"].ToString(),
                        Email = row["Email"].ToString(),
                        Address = row["Address"].ToString(),
                        UserName = row["UserName"].ToString(),
                        Role_Id = (int)row["Role_Id"],
                        Status_Id = (int)row["Status_Id"],
                        EncryptionKey = row["EncryptionKey"].ToString()
                    };
                    return User;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add(DTO_Add RegisterModel)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("SEC_RegisterUser", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@FirstName", RegisterModel.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", RegisterModel.LastName);
                    cmd.Parameters.AddWithValue("@Email", RegisterModel.Email);
                    cmd.Parameters.AddWithValue("@Address", RegisterModel.Address);
                    cmd.Parameters.AddWithValue("@Password", RegisterModel.Password);
                    cmd.Parameters.AddWithValue("@UserName", RegisterModel.UserName);
                    cmd.Parameters.AddWithValue("@Role_Id", RegisterModel.Role_Id);
                    cmd.Parameters.AddWithValue("@Status_Id", RegisterModel.Status_Id);
                    cmd.Parameters.AddWithValue("@InsertedBy", RegisterModel.InsertedBy);
                    cmd.Parameters.AddWithValue("@EncryptionKey", RegisterModel.EncryptionKey);


                    SqlParameter RetValue = cmd.Parameters.Add("RetValue", SqlDbType.Int);
                    RetValue.Direction = ParameterDirection.ReturnValue;


                    conn.Open();
                    cmd.ExecuteNonQuery();
                    int UserId = (int)RetValue.Value;
                    conn.Close();
                    cmd.Dispose();
                    if (UserId > 0)
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetFeatureToRoleByRoleId", new[] { (Object)RegisterModel.Role_Id });
                        List<DTO_FeatureToUser> result = ds.Tables[0].AsEnumerable()
                       .Select(dataRow => new DTO_FeatureToUser
                       {
                           id = UserId,
                           AF_Id = dataRow.Field<int>("AF_Id")
                       }).ToList();

                        foreach (DTO_FeatureToUser item in result)
                        {
                            object[] array = new[] { (object)item.id, (object)item.AF_Id };
                            SqlHelper.ExecuteNonQuery(conn, "SEC_AddFeatureToUser", array);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)searchTerm };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_SearchUser", array);

                    List<DTO_GetAll> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO_GetAll
                        {
                            id = dataRow.Field<int>("id"),
                            FirstName = dataRow.Field<String>("FirstName"),
                            LastName = dataRow.Field<String>("LastName"),
                            Email = dataRow.Field<String>("Email"),
                            Address = dataRow.Field<String>("Address"),
                            UserName = dataRow.Field<String>("UserName"),
                            Role_Id = dataRow.Field<int>("Role_Id"),
                            Status_Id = dataRow.Field<int>("Status_Id")
                        }).ToList();

                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(DTO_Update userData)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("SEC_UpdateUser", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", userData.id);
                    cmd.Parameters.AddWithValue("@FirstName", userData.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", userData.LastName);
                    cmd.Parameters.AddWithValue("@Email", userData.Email);
                    cmd.Parameters.AddWithValue("@Address", userData.Address);
                    cmd.Parameters.AddWithValue("@Password", userData.Password);
                    cmd.Parameters.AddWithValue("@UserName", userData.UserName);
                    cmd.Parameters.AddWithValue("@Role_Id", userData.Role_Id);
                    cmd.Parameters.AddWithValue("@Status_Id", userData.Status_Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", userData.UpdatedBy);
                    cmd.Parameters.AddWithValue("@EncryptionKey", userData.EncryptionKey);


                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Bit);
                    Value.Direction = ParameterDirection.ReturnValue;


                    conn.Open();
                    cmd.ExecuteNonQuery();
                    int result = (int)Value.Value;
                    conn.Close();

                    if (result > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(DTO_Delete UserData)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("SEC_DeleteUserById", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@User_id", UserData.id);


                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Bit);
                    Value.Direction = ParameterDirection.ReturnValue;


                    conn.Open();
                    cmd.ExecuteNonQuery();
                    int result = (int)Value.Value;
                    conn.Close();

                    if (result > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public List<DTO_GetAll> GetAll() 
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetAllUsers", null);

                    List<DTO_GetAll> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO_GetAll
                        {
                            id = dataRow.Field<int>("id"),
                            FirstName = dataRow.Field<String>("FirstName"),
                            LastName = dataRow.Field<String>("LastName"),
                            Email = dataRow.Field<String>("Email"),
                            Address = dataRow.Field<String>("Address"),
                            Password = dataRow.Field<String>("Password"),
                            UserName = dataRow.Field<String>("UserName"),
                            Role_Id = dataRow.Field<int>("Role_Id"),
                            Status_Id = dataRow.Field<int>("Status_Id")
                        }).ToList();

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsEmailAlreadyExist(string email)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)email };
                    DataSet ds = SqlHelper.ExecuteDataset(connection, "SEC_GetUserByEmail", array);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsUserNameAlreadyExist(string username)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)username };
                    DataSet ds = SqlHelper.ExecuteDataset(connection, "SEC_GetUserByUserName", array);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return true;
            }

        }

    }
}