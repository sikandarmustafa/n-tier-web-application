﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DTO.DTO_Role;

namespace DAL.Security
{
    public class SEC_Role: SEC_Interface<DTO_Add, DTO_Update, DTO_Delete, DTO_GetAll>
    {
        string CS = DAL_DbConnection.GetConnection();

        public List<DTO_GetAll> GetAll()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetAllRoles", null);

                    List<DTO_GetAll> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO_GetAll
                        {
                            Role_Title = dataRow.Field<String>("Role_Title"),
                            Role_Id = dataRow.Field<int>("Role_Id"),
                            Status_Id = dataRow.Field<int>("Status_Id")
                        }).ToList();

                    return result;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Add(DTO_Add role)
        {
            try
            {

                using (SqlConnection connection = new SqlConnection(CS))
                {
                    using (SqlCommand cmd = new SqlCommand("SEC_AddRole", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@Role_Title", role.Role_Title);
                        cmd.Parameters.AddWithValue("@Status_Id", role.Status_Id);
                        cmd.Parameters.AddWithValue("@InsertedBy", role.InsertedBy);

                        SqlParameter RetValue = cmd.Parameters.Add("Value", SqlDbType.Bit);
                        RetValue.Direction = ParameterDirection.ReturnValue;


                        connection.Open();
                        cmd.ExecuteNonQuery();
                        connection.Close();

                        if ((int)RetValue.Value > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)searchTerm };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_SearchRole", array);

                    List<DTO_GetAll> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO_GetAll
                        {
                            Role_Id = (int)dataRow.Field<int>("Role_Id"),
                            Role_Title = dataRow.Field<String>("Role_Title")
                        }).ToList();

                    return result;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(DTO_Update role)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_UpdateRole", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Role_Id", role.Role_Id);
                    cmd.Parameters.AddWithValue("@Role_Title", role.Role_Title);
                    cmd.Parameters.AddWithValue("@UpdatedBy", role.UpdatedBy);
                    cmd.Parameters.AddWithValue("@Status_Id", role.Status_Id);


                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Bit);
                    Value.Direction = ParameterDirection.ReturnValue;

                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }  

        public bool IsAlreadyExist(string role)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)role };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetRoleByTitle", array);
                    int result = ds.Tables[0].Rows.Count;
                    
                    if(result > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetById(int Role_Id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)Role_Id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetRoleById", array);
                    DataTable result = ds.Tables[0];

                    DataRow row = result.Rows[0];

                    DTO_GetAll User = new DTO_GetAll()
                    {
                        Role_Title = row["Role_Title"].ToString(),
                        Role_Id = (int)row["Role_Id"],
                        Status_Id = (int)row["Status_Id"]
                    };
                    return User;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int GetId(string role)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)role };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetRoleByTitle", array);
                    DataRow row = ds.Tables[0].Rows[0];

                    return (int)row["Role_Id"];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(DTO_Delete Role)
        {
            try
            {

                using (SqlConnection conn = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("SEC_DeleteRole", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Role_Id", Role.Role_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Bit);
                    Value.Direction = ParameterDirection.ReturnValue;


                    conn.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    conn.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }

        }
    }
}
