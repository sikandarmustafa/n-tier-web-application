﻿using System.Collections.Generic;

namespace DAL.Security
{
    interface SEC_Interface<T, U, V, X>
    {
        bool Add(T data);

        bool Update(U data);

        bool Delete(V data);

        List<X> GetAll();

        X GetById(int id);

        List<X> Search(string SearchTerm);
    }
}
