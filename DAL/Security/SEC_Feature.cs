﻿using DTO.DTO_Feature;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DAL.Security
{
    public class SEC_Feature: SEC_Interface<DTO_Add, DTO_Update, DTO_Delete, DTO_GetAll>
    {
        string CS = DAL_DbConnection.GetConnection();
        public List<DTO_GetAll> GetAll()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetAllFeatures", null);
                    List< DTO_GetAll> result = ds.Tables[0].AsEnumerable().Select(dataRow => new DTO_GetAll
                    {
                        Feature_Id = dataRow.Field<int>("Feature_Id"),
                        Feature_Name = dataRow.Field<String>("Feature_Name"),
                        Feature_Url = dataRow.Field<String>("Feature_URL"),
                        Feature_Icon = dataRow.Field<String>("Feature_Icon"),
                        Status_Id = dataRow.Field<int>("Status_Id"),
                        IsSelected = false
                    }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add(DTO_Add feature)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_AddFeature", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Feature_Name", feature.Feature_Name);
                    cmd.Parameters.AddWithValue("@Feature_Url", feature.Feature_Url);
                    cmd.Parameters.AddWithValue("@Feature_Icon", feature.Feature_Icon);
                    cmd.Parameters.AddWithValue("@InsertedBy", feature.InsertedBy);
                    cmd.Parameters.AddWithValue("@Status_Id", feature.Status_Id);


                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(DTO_Update feature)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("SEC_UpdateFeature", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Feature_Id", feature.Feature_Id);
                    cmd.Parameters.AddWithValue("@Feature_Name", feature.Feature_Name);
                    cmd.Parameters.AddWithValue("@Feature_Url", feature.Feature_Url);
                    cmd.Parameters.AddWithValue("@Feature_Icon", feature.Feature_Icon);
                    cmd.Parameters.AddWithValue("@UpdatedBy", feature.UpdatedBy);
                    cmd.Parameters.AddWithValue("@Status_Id", feature.Status_Id);


                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Bit);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool Delete(DTO_Delete feature)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_DeleteFeature", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Feature_Id", feature.Feature_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)searchTerm };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_SearchFeatures", array);

                    List< DTO_GetAll> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO_GetAll
                        {
                            Feature_Id = (int)dataRow.Field<int>("Feature_Id"),
                            Feature_Name = dataRow.Field<string>("Feature_Name"),
                            Feature_Url = dataRow.Field<string>("Feature_Url"),
                            Feature_Icon = dataRow.Field<string>("Feature_Icon"),
                            Status_Id = dataRow.Field<int>("Status_Id")
                        }).ToList();

                    return result;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetById(int id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetFeatureById", array);

                    DataRow row = ds.Tables[0].Rows[0];

                    DTO_GetAll result = new DTO_GetAll()
                    {
                        Feature_Id = (int)row["Feature_Id"],
                        Feature_Name = row["Feature_Name"].ToString(),
                        Feature_Url = row["Feature_Url"].ToString(),
                        Feature_Icon = row["Feature_Icon"].ToString(),
                        Status_Id = (int)row["Status_Id"]
                    };
                    return result;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool IsAlreadyExists(DTO_Add feature)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    
                    object[] array = new[] { (object)feature.Feature_Name, (object)feature.Feature_Url };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_IsFeatureAlreadyExist", array);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
