﻿using System;
using System.Collections.Generic;
using System.Linq;
using DTO.DTO_Action;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Security
{
    public class SEC_Action: SEC_Interface<DTO_Add, DTO_Update, DTO_Delete, DTO_GetAll>
    {
        string CS = DAL_DbConnection.GetConnection();

        public bool Add(DTO_Add action)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_AddAction", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action_Name", action.Action_Name);
                    cmd.Parameters.AddWithValue("@InsertedBy", action.InsertedBy);
                    cmd.Parameters.AddWithValue("@Status_Id", action.Status_Id);


                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(DTO_Update action)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_UpdateAction", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action_Id", action.Action_Id);
                    cmd.Parameters.AddWithValue("@Action_Name", action.Action_Name);
                    cmd.Parameters.AddWithValue("@UpdatedBy", action.UpdatedBy);
                    cmd.Parameters.AddWithValue("@Status_Id", action.Status_Id);


                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public bool Delete(DTO_Delete action)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CS))
                {

                    SqlCommand cmd = new SqlCommand("SEC_DeleteAction", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Action_Id", action.Action_Id);

                    SqlParameter Value = cmd.Parameters.Add("Value", SqlDbType.Int);
                    Value.Direction = ParameterDirection.ReturnValue;


                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    if ((int)Value.Value > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<DTO_GetAll> Search(string searchTerm)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)searchTerm };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_SearchActions", array);

                    List< DTO_GetAll> result = ds.Tables[0].AsEnumerable()
                        .Select(dataRow => new DTO_GetAll
                        {
                            Action_Id = (int)dataRow.Field<int>("Action_Id"),
                            Action_Name = dataRow.Field<string>("Action_Name"),
                            Status_Id = dataRow.Field<int>("Status_Id")
                        }).ToList();

                    return result;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DTO_GetAll GetById(int id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetActionById", array);

                    DataRow row = ds.Tables[0].Rows[0];

                    DTO_GetAll result = new DTO_GetAll()
                    {
                        Action_Id = (int)row["Action_Id"],
                        Action_Name = row["Action_Name"].ToString(),
                        Status_Id = (int)row["Status_Id"]

                    };
                    return result;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<DTO_GetAll> GetActionForFeature(int Feature_Id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    object[] array = new[] { (object)Feature_Id };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetActionsForFeature", array);
                    List< DTO_GetAll> result = ds.Tables[0].AsEnumerable().Select(dataRow => new DTO_GetAll
                    {
                        Action_Id = dataRow.Field<int>("Action_Id"),
                        Action_Name = dataRow.Field<string>("Action_Name"),
                        AF_Id = dataRow.Field<int>("AF_Id"),
                        IsSelected = false
                    }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTO_GetAll> GetAll()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_GetAllActions", null);
                    List<DTO_GetAll> result = ds.Tables[0].AsEnumerable().Select(dataRow => new DTO_GetAll
                    {
                        Action_Id = dataRow.Field<int>("Action_Id"),
                        Action_Name = dataRow.Field<String>("Action_Name"),
                        Status_Id = dataRow.Field<int>("Status_Id"),
                        IsSelected = false
                    }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsAlreadyExists(DTO_Add action)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(CS))
                {
                    /// string Query = string.Format("select Action_Id from Actions where Action_Name = '{0}'", action.Action_Name);
                    object[] array = new[] {(object)action.Action_Name };
                    DataSet ds = SqlHelper.ExecuteDataset(conn, "SEC_IsActionAlreadyExist", array);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
